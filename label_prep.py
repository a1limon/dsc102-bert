from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.types import IntegerType


def main(spark):
    """
    Main function outputs a parquet table from EMR to s3 bucket
    :return: saves parquet table to s3
    """
    # df_features = spark.read.parquet('s3://ds102-bert-bucket/historical_data.parquet').drop(
    #     '__index_level_0__')  # read feature/origination data
    monthly = spark.read.option('header', False).option("delimiter", "|").csv(
        's3://ds102-bert-scratch/historical_data_time*.txt')  # read monthly data
    df_monthly = monthly[["_c0", "_c3", "_c8"]]  # use only relevant features
    df_temp = df_monthly.withColumn("_c3", df_monthly["_c3"].cast(IntegerType()))  # cast type
    df_gen_label = df_temp.withColumn('label', F.when((F.col("_c3") >= 90) | ((F.col("_c8") == '03') | (F.col("_c8") == '06') | (
                F.col("_c8") == '09')), 1).otherwise(0)
    )  # generate label
    df_labels = df_gen_label.select('_c0', 'label').groupby('_c0').agg(F.max('label').alias('label'))
    # df_output = df_features.join(df_labels, df_features.loan_seq_num == df_labels._c0, 'left')
    return df_labels.write.mode('overwrite').parquet('s3://ds102-bert-scratch/historical_data_time_label.parquet')


if __name__ == '__main__':
    spark = SparkSession.builder.appName("label-prep").getOrCreate()
    main(spark)
    print("output parquet written to s3")
