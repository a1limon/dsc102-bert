# import os
# import glob
# import s3fs
import dask.dataframe as dd
from dask import delayed
from distributed import Client, LocalCluster, performance_report
# import numpy as np
from dask_ml import impute
from dask_ml.preprocessing import OneHotEncoder
# from dask_ml.preprocessing import StandardScaler
# from dask_ml.preprocessing import OrdinalEncoder
# from dask_ml.preprocessing import Categorizer
# from dask_ml.compose import ColumnTransformer


col_names1 = ["credit_score", "first_payment_date", "first_time_homebuyer", "maturity_date", "MSA", "MI_percent",
              "num_units", "occupancy_status", "CLTV", "DTI", "UPB", "LTV", "orig_interest_rate", "channel", "PPM",
              "amortize_type", "property_state", "property_type", "postal_code", "loan_seq_num", "loan_purpose",
              "orig_loan_term", "num_borrowers", "seller_name", "servicer_name", "SCF", "preharp_loan_seq_num",
              "program_indicator", "harp_indicator", "prop_val_method", "io_indicator"]

# col_names2 = ["loan_seq_num", "monthly_reporting_per", "current_actual_UPB", "CLDS", "loan_age", "RMTLM",
#               "repurchase flag", "mod_flag", "zero_bal_code", "zero_bal_eff_date", "curr_interest_rate",
#               "curr_deferred_upb", "DDLPI", "mi_recoveries", "net_sales_proceeds", "non_mi_recoveries", "expenses",
#               "legal_costs", "MAPC", "taxes_and_insurance", "misc_expenses", "actual_loss_calc", "mod_cost",
#               "step_mod_flag", "deferred_payment_plan", "ELTV", "zero_bal_removal_upb", "delinquent_accrued_int",
#               "DDD", "BASD"]  # zero_bal_code 03,06,09 indicates loan is defaulted

mode_imputer = impute.SimpleImputer(strategy='most_frequent')

@delayed
def fit_transform_mode(d):
    """
    Function that imputes the mode using dask_ml imputer
    :param d: input data
    :return: transformed data
    """
    return mode_imputer.fit_transform(d)


@delayed
def setdf_impute(df, a, b, c):
    """
    Function that resets dataframe after imputation
    :param df: input dataframe
    :param a: credit_score column
    :param b: num_borrowers column
    :param c: num_units column
    :return: dataframe with transformed columns
    """
    df.iloc[:, [0]] = a  # credit_score
    df.iloc[:, [16]] = b  # num_borrowers
    df.iloc[:, [3]] = c  # num_units
    return df


# # encoding categorical variables, and qualitative variables
# scaler = StandardScaler()
# ordinal = OrdinalEncoder()
# cat = Categorizer()
#
# ct = ColumnTransformer([
#     ('scaler', scaler, ['credit_score', 'CLTV', 'DTI', 'UPB', 'orig_loan_term']),
#     ('categorizer', cat, ['first_time_homebuyer', 'MSA', 'num_units', 'occupancy_status', 'channel', 'PPM',
#                           'amortize_type', 'property_type', 'loan_purpose', 'num_borrowers', 'SCF',
#                           'program_indicator', 'harp_indicator', 'io_indicator'])
# ])
@delayed
def df_fill(df, col, val):
    """
    Function that fills null values in dataframe column
    :param df: input dataframe (origination data)
    :param col: column to replace values
    :param val: value to replace with
    :return: dataframe column with missing objects filled
    """
    return df[col].fillna(val)


@delayed
def mask_cols(df, col, to_replace, val):
    """
    Function that replaces values in dataframe
    :param df: input dataframe (origination data)
    :param col: column to replace values
    :param to_replace: value to be replaced
    :param val: replacement value
    :return: column with specified values replaced
    """
    return df[col].mask(df[col] == to_replace, val)


@delayed
def setdf_fill(df, rep1, rep2, rep3):
    """
    Function that updates given dataframe with filled columns
    :param df: input dataframe
    :param rep1: first column to be replaced/MSA
    :param rep2: second column to be replaced/SCF
    :param rep3: third column to be replaced/harp_indicator
    :return: dataframe with replaced values
    """
    df['MSA'] = rep1
    df['SCF'] = rep2
    df['harp_indicator'] = rep3
    return df


@delayed   
def setdf_mask(df, col1, col2, col3, col4, col5, col6, col7):
    """
    Function that takes columns to be replaced
    :param df: input dataframe (origination data)
    :param col1: first_time_homebuyer
    :param col2: occupancy_status
    :param col3: DTI
    :param col4: channel
    :param col5: property_type
    :param col6: loan_purpose
    :param col7: program_indicator
    :return: updated dataframe with replaced values in given columns
    """
    df.first_time_homebuyer = col1
    df.occupancy_status = col2
    df.DTI = col3
    df.channel = col4
    df.property_type = col5
    df.loan_purpose = col6
    df.program_indicator = col7
    return df


# preprocessing
def preprocess():
    """ Preprocessing function
    :return: preprocessed dataframe
    """
    # loan origination data, might need to change file path
    df1 = dd.read_csv('s3://ds102-bert-scratch/historical_data_20*.txt', sep='|', header=None,
                      dtype={25: 'object', 26: 'object', 27: 'object', 28: 'object'}, names=col_names1,
                      usecols=['credit_score', 'first_time_homebuyer', 'MSA', 'num_units', 'occupancy_status', 'CLTV',
                               'DTI', 'UPB', 'orig_interest_rate', 'channel', 'PPM', 'amortize_type', 'property_type',
                               'loan_seq_num', 'loan_purpose', 'orig_loan_term', 'num_borrowers', 'SCF',
                               'program_indicator', 'harp_indicator', 'io_indicator'])
    # null value replacements
    to_rep1 = df_fill(df1, 'MSA', 0)  # 0 for area in which mortgaged property is neither an MSA/metro division or unknown
    to_rep2 = df_fill(df1, 'SCF', 'N')  # indicating not a super conforming mortgage
    to_rep3 = df_fill(df1, 'harp_indicator', 'N')  # indicator identifies whether loan was harp or not, Y=harp, N=non-harp
    df1 = setdf_fill(df1, to_rep1, to_rep2, to_rep3)
    # mode imputations
    mode1 = fit_transform_mode(df1.iloc[:, [0]])  # credit score
    mode2 = fit_transform_mode(df1.iloc[:, [16]])  # num_borrowers
    mode3 = fit_transform_mode(df1.iloc[:, [3]])  # num_units
    df1 = setdf_impute(df1, mode1, mode2, mode3)
    # mask replacements
    mask1 = mask_cols(df1, 'first_time_homebuyer', 9, 'NA')
    mask2 = mask_cols(df1, 'occupancy_status', 9, 'NA')
    mask3 = mask_cols(df1, 'DTI', 999, 66)
    mask4 = mask_cols(df1, 'channel', 9, 'NA')
    mask5 = mask_cols(df1, 'property_type', 99, 'NA')
    mask6 = mask_cols(df1, 'loan_purpose', 9, 'NA')
    mask7 = mask_cols(df1, 'program_indicator', 9, 'N')
    df1 = setdf_mask(df1, mask1, mask2, mask3, mask4, mask5, mask6, mask7)
    # df1 = df.compute()
    df1 = df1[(df1.CLTV >= 1) & (df1.CLTV <= 998)]
    return df1


def cat_encode(df):
    cat_cols = list(df.columns[df.dtypes == 'object'])
    df[cat_cols] = df[cat_cols].astype("category")
    cat_cols.remove('loan_seq_num')
    enc = OneHotEncoder(sparse=False)
    enc = enc.fit(df[cat_cols])
    df_onehot = enc.transform(df[cat_cols])
    df_onehot.reset_index(drop=True, inplace=True)
    df.reset_index(drop=True, inplace=True)
    df = dd.merge(df, df_onehot, left_index=True, right_index=True, how='outer')
    df = df.drop(columns=cat_cols)
    return df.to_parquet('s3://ds102-bert-scratch/historical_data.parquet', engine='pyarrow')


# feature engineering
# hand-craft features, read dataset description, and about loan underwriting
# idea for DTI, make a DTI_cat variable, encode ranges, e.g. DTI 0-35% Good, 35-50% OK, 50%- BAD
# extract time series features, for example first_payment_date would be split to first_payment_year, first_payment_month


# preprocesses data and writes a parquet file to store in S3
if __name__ == '__main__':
    client = Client()
    with performance_report(filename="dask-report3.html"):
        df1 = preprocess().compute()
        cat_encode(df1)
        print("written to parquet")
    client.shutdown()
