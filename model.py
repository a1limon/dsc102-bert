from dask.distributed import Client
import dask.dataframe as dd
from dask_ml.xgboost import XGBClassifier
from dask_ml.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import GridSearchCV



def main():
    df_features = dd.read_parquet('s3://ds102-bert-scratch/historical_data.parquet')  # read origination data/features
    df_labels = dd.read_parquet('s3://ds102-bert-scratch/historical_data_time_label.parquet/')  # read label data
    df_labels['_c0'] = df_labels['_c0'].astype("category")  # cast to match primary key type in origination data
    df_output = df_labels.merge(df_features, left_on='_c0', right_on='loan_seq_num', how='left')  # merge data
    df_output = df_output.dropna()  # drop null rows
    X = df_output.drop(columns=['_c0', 'label', 'loan_seq_num'])
    y = df_output.label

    num_pos = len(df_output[df_output.label == 1])
    num_neg = len(df_output[df_output.label == 0])
    scale_pos_weight = (num_neg/num_pos)
    Xtrain, Xtest, ytrain, ytest = train_test_split(X, y, test_size=0.2, shuffle=False)  # 80/20 split
    xgb_params = {'learning_rate': 0.01, 'n_estimators': 300, 'max_depth': 10, 'min_child_weight': 1, 'lambda': 0.001,
                  'subsample': 0.5, 'colsample_bytree': 0.8, 'objective': 'binary:logistic', 'nthread': -1,
                  'scale_pos_weight': scale_pos_weight, 'seed': 27, 'tree_method': 'approx'}

    model = XGBClassifier(**xgb_params)
    print("fitting model...")
    model.fit(Xtrain, ytrain, eval_metric='auc')  # fit model for distributed training
    print("making training set predictions...")
    ypred_tr = model.predict_proba(Xtrain)
    print('train auc:', roc_auc_score(ytrain, ypred_tr))
    print("making test set predictions...")
    ypred = model.predict_proba(Xtest)
    print('test auc:', roc_auc_score(ytest, ypred))


if __name__ == '__main__':
    client = Client()
    main()
    client.shutdown()
