# dsc102-bert

## Machine Learning Systems Craft Demonstration
### Deliverables:
- S3 bucket
- screenshot of Dask UI running `DaskUI_screenshot.png`
- Feature Engineering
    - `feature_prep.py`
    - bash file for EC2 set up `ec2_initialization.sh`
- Label Generation
    - `label_prep.py`
    - bash file for EMR setup `emr_initialization.sh`
- Graph indicating a data flow that would maximize parallelism for label prep and feature engineering modules `DataFlowGraph.png`
- Setup details
- Discussion
